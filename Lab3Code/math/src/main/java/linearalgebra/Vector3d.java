package linearalgebra;
//Mathews Velez
//1912607
public class Vector3d {
    public double x;
    public double y;
    public double z;
    
    
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double getMag(){
        return (Math.sqrt((Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z,2))));
    }

    public double dot(Vector3d other){
        return (this.x * other.x)+(this.y * other.y)+(this.z * other.z);
    }

    public Vector3d add(Vector3d other){
        return new Vector3d(this.x + other.x, this.y + other.y, this.z + other.z);
    }
}
