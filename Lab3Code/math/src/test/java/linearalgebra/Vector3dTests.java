package linearalgebra;
//Mathews Velez
//1912607
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests {
    @Test
    public void testGetMethods(){
        Vector3d getTester = new Vector3d(2, 3, 4);
        assertEquals(2, getTester.getX(),0);
        assertEquals(3, getTester.getY(), 0);
        assertEquals(4, getTester.getZ(),0);
    }
    
    @Test
    public void testMagMethod(){
        Vector3d magTester = new Vector3d(2, 6, 3);
        assertEquals(7, magTester.getMag(),0);
    }

    @Test
    public void testDotProduct(){
        Vector3d test1 = new Vector3d(2, 6, 3);        
        Vector3d test2 = new Vector3d(4, 3, 5);
        assertEquals(41, test1.dot(test2),0);
    }

    @Test
    public void testadd(){
        Vector3d test1 = new Vector3d(2, 6, 3);        
        Vector3d test2 = new Vector3d(4, 3, 5);
        Vector3d awsr = test1.add(test2);
        assertEquals(awsr.x, test1.x + test2.x,0);
        assertEquals(awsr.y, test1.y + test2.y,0);
        assertEquals(awsr.z, test1.z + test2.z,0);
    }

}

